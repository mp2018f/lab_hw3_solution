import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  results: any;
  results_backup: any;
  keyword: string;
  filterKeyword: string;

  constructor(public navCtrl: NavController, private toast: ToastController, private http: Http) {
  }
    
  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  onSearch() {
    this.navCtrl.setRoot(HomePage);
  }
  onGo() {
    this.http.get("http://www.omdbapi.com/?s="+this.keyword+"&apikey=1bcec5ea")
    .map(res=>res.json())
    .subscribe( data=>
      { console.log(data); 		
        if(data.Response == "False") {
          this.results=null;
          this.toast.create(
            {message: "No movie was found.", 
            duration: 2000}
          ).present()
        } else {
          this.results = data.Search;	
          this.results_backup = this.results;	
        }
      }
    ); 
  }

  onFilter() {

    this.results = this.results_backup.filter(p=>p.Title.toLowerCase().includes(this.filterKeyword.toLowerCase())); 
    // this.results = this.results_backup.filter(p=>(p.Title+" "+p.Year).toLowerCase().includes(this.filterKeyword.toLowerCase())); 
  }
}
