import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  result: any;
  keyword: string;

  constructor(public navCtrl: NavController, private toast: ToastController, private http: Http) {
    
  }

  onSearch() {
    this.navCtrl.setRoot('AboutPage');
  }

  onGo() {
    this.http.get("http://www.omdbapi.com/?t="+this.keyword+"&apikey=1bcec5ea")
    .map(res=>res.json())
    .subscribe( data=>
      { console.log(data); 		
        this.result = data;		
        if(this.result.Response == "False") {
          this.result=null;
          this.toast.create(
            {message: "No movie was found.", 
            duration: 2000}
          ).present()
        }
      }
    ); 
  }
}
